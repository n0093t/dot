# Тестовое задание

## Описание
Приложение состоит из 3-х сервисов:

- `API` - реализует методы для взаимодействия со счетами клиентов.
- `Scheduler` - С заданной периодичностью (настраивается переменными окружения) отправляет задачи на снятие холда в `RabbitMQ`.
- `Consumer` - Слушает очередь `RabbitMQ` и выполняет снятие холда при получении сообщения от сервиса `Scheduler`.

[Схема взаимодействия сервисов](readme/service-interaction.png)

## CLI 
##### Сборка образов
```bash
./make.sh build [service_name service_name ...]
```
* service_name:
    * api
    * scheduler
    * consumer

##### Запуск всех контейнеров
```bash
./make.sh up
```
После выполнения команды API будет доступен на localhost:80

##### Остановка всех контейнеров с последующим удалением
```bash
./make.sh down
```

##### Остановка контейнеров
```bash
./make.sh stop [service_name service_name ...]
```

* service_name:
    * api
    * scheduler
    * consumer
    * rabbitmq
    * postgres
    * nginx

##### Тестирование сервиса
```bash
./make.sh test [service_name]
```

* service_name:
    * api

##### Просмотр логов сервиса
```bash
./make.sh logs service_name
```

* service_name:
    * api
    * scheduler
    * consumer
    * rabbitmq
    * postgres
    * nginx

## Документация по использованию API
[Swagger](swagger.yml) 

## PS
Из задания не ясно как влияет состояние счета (открыт/закрыт) на операцию снятия холда, 
поэтому операция снятия холда игнорирует закрытые счета.
Не описано поведение, при котором баланс счета может стать отрицателным в результате снятия холда, 
поэтому снимаю в минус.       
