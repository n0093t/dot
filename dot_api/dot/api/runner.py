import uvloop
from aiohttp import web

from dot.api.health_check.checker import AlivenessChecker
from dot.api.health_check.checkers.postgres import PostgresChecker
from dot.api.rest.middleware import MIDDLEWARES
from dot.api.rest.routes import ROUTES
from dot.api.service import AccountService
from dot.api.storage.postgres import PostgresStorage


class Runner:
    def __init__(self, api_settings, storage_settings):
        self.__api_settings = api_settings
        self.__storage_settings = storage_settings
        self.__app = None
        self.__storage = None
        self.__account_service = None

    async def __on_shutdown(self, app):  # pylint: disable=W0613
        await self.__storage.close()

    def __setup_storage(self):
        self.__storage = PostgresStorage(
            self.__storage_settings.dsn,
            pool_min_size=self.__storage_settings.pool_min_size,
            pool_max_size=self.__storage_settings.pool_max_size,
            timeout=self.__storage_settings.timeout,
        )

    def __setup_account_service(self):
        self.__account_service = AccountService(self.__storage)

    @staticmethod
    def __setup_loop():
        uvloop.install()

    def __setup_app(self):
        app = web.Application(
            middlewares=MIDDLEWARES
        )

        app.on_shutdown.append(self.__on_shutdown)
        app.add_routes(ROUTES)

        app['storage'] = self.__storage
        app['account_service'] = self.__account_service

        self.__app = app

    def __setup_health_checkers(self):
        checkers = [
            PostgresChecker(self.__storage_settings.dsn),
        ]

        self.__app['aliveness_checker'] = AlivenessChecker(checkers)

    def __setup(self):
        self.__setup_loop()
        self.__setup_storage()
        self.__setup_account_service()
        self.__setup_app()
        self.__setup_health_checkers()

    def run(self):
        self.__setup()
        web.run_app(self.__app)
