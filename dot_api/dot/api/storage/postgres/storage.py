import sys
from contextlib import asynccontextmanager
from contextvars import ContextVar
from decimal import Decimal
from uuid import uuid4

from aiopg.sa import create_engine
from sqlalchemy import sql

from dot.api.common.dto import AccountDTO, SubscriberDTO
from dot.api.storage.exceptions import (
    AccountDoesNotExist,
    SubscriberDoesNotExist,
)
from dot.api.storage.interface import AbstractStorage
from dot.api.storage.postgres.models import account, subscriber


shared_connection = ContextVar('shared_connection', default=None)


class PostgresStorage(AbstractStorage):
    def __init__(self, dsn: str, *, pool_min_size: int = 1,
                 pool_max_size: int = 20, timeout: int = 10):
        self.__engine = None
        self.__engine_coroutine = create_engine(
            dsn=dsn,
            minsize=pool_min_size,
            maxsize=pool_max_size,
            timeout=timeout
        )

    async def close(self):
        if self.__engine is not None:
            await self.__engine.__aexit__(*sys.exc_info())

    @property
    async def engine(self):
        if self.__engine is None:
            self.__engine = await self.__engine_coroutine

        return self.__engine

    @asynccontextmanager
    async def __connection(self):
        engine = await self.engine
        conn = shared_connection.get()

        if conn is None:
            try:
                async with engine.acquire() as conn:
                    shared_connection.set(conn)
                    yield conn
            finally:
                shared_connection.set(None)
        else:
            yield conn

    @asynccontextmanager
    async def atomic(self):
        async with self.__connection() as conn:
            async with conn.begin() as trn:
                yield conn, trn

    async def get_account_by_id(self, account_id: uuid4,
                                for_update=False) -> AccountDTO:
        query = sql.select(
            [
                account,
                subscriber
            ],
            use_labels=True
        ).select_from(
            account.join(subscriber)
        ).where(
            account.c.id == account_id
        )

        if for_update:
            query = query.with_for_update()

        async with self.__connection() as conn:
            cur = await conn.execute(query)
            account_obj = await cur.fetchone()

            if account_obj is None:
                raise AccountDoesNotExist(account_id)

        subscriber_dto = SubscriberDTO(
            id=account_obj.subscriber_id,
            full_name=account_obj.subscriber_full_name
        )

        return AccountDTO(
            id=account_obj.account_id,
            owner=subscriber_dto,
            balance=account_obj.account_balance,
            hold=account_obj.account_hold,
            open=account_obj.account_open
        )

    async def add(self, account_id: uuid4, amount: Decimal) -> AccountDTO:
        async with self.__connection() as conn:
            update_query = sql.update(
                account,
                values={
                    account.c.balance: account.c.balance + amount
                },
            ).where(
                account.c.id == account_id,
            ).returning(
                account.c.id,
                account.c.owner_id,
                account.c.balance,
                account.c.hold,
                account.c.open,
            ).cte('update_account')

            query = sql.select(
                [
                    update_query,
                    subscriber
                ],
                use_labels=True
            ).select_from(
                update_query.join(subscriber)
            )

            cur = await conn.execute(query)
            account_obj = await cur.fetchone()

            subscriber_dto = SubscriberDTO(
                id=account_obj.subscriber_id,
                full_name=account_obj.subscriber_full_name,
            )

            return AccountDTO(
                id=account_obj.update_account_id,
                owner=subscriber_dto,
                balance=account_obj.update_account_balance,
                hold=account_obj.update_account_hold,
                open=account_obj.update_account_open,
            )

    async def subtract(self, account_id: uuid4, amount: Decimal):
        async with self.__connection() as conn:
            update_query = sql.update(
                account,
                values={
                    account.c.hold: account.c.hold + amount
                }
            ).where(
                account.c.id == account_id
            ).returning(
                account.c.id,
                account.c.owner_id,
                account.c.balance,
                account.c.hold,
                account.c.open,
            ).cte('update_account')

            query = sql.select(
                [
                    update_query,
                    subscriber
                ],
                use_labels=True
            ).select_from(
                update_query.join(subscriber)
            )

            cur = await conn.execute(query)
            account_obj = await cur.fetchone()

            subscriber_dto = SubscriberDTO(
                id=account_obj.subscriber_id,
                full_name=account_obj.subscriber_full_name,
            )

            return AccountDTO(
                id=account_obj.update_account_id,
                owner=subscriber_dto,
                balance=account_obj.update_account_balance,
                hold=account_obj.update_account_hold,
                open=account_obj.update_account_open,
            )

    async def create_subscriber(self,
                                subscriber_id: uuid4,
                                full_name: str) -> SubscriberDTO:

        async with self.__connection() as conn:
            cursor = await conn.execute(
                subscriber.insert().returning(  # pylint: disable=E1120
                    sql.literal_column('*')
                ),
                {
                    'id': subscriber_id,
                    'full_name': full_name,
                }
            )

            subscriber_obj = await cursor.fetchone()

            return SubscriberDTO(
                id=subscriber_obj.id,
                full_name=subscriber_obj.full_name,
            )

    async def get_subscriber_by_id(self, subscriber_id: uuid4) -> SubscriberDTO:
        query = sql.select(
            [subscriber],
        ).where(
            subscriber.c.id == subscriber_id
        )

        async with self.__connection() as conn:
            cursor = await conn.execute(query)
            subscriber_obj = await cursor.fetchone()

            if subscriber_obj is None:
                raise SubscriberDoesNotExist(subscriber_id)

        return SubscriberDTO(
            id=subscriber_obj.id,
            full_name=subscriber_obj.full_name
        )

    async def create_account(self, account_id: uuid4, owner_id: uuid4,
                             balance: Decimal, hold: Decimal,
                             is_opened: bool) -> AccountDTO:

        async with self.__connection() as conn:
            subscriber_dto = await self.get_subscriber_by_id(
                subscriber_id=owner_id
            )
            cursor = await conn.execute(
                account.insert().returning(  # pylint: disable=E1120
                    sql.literal_column('*')
                ),
                {
                    'id': account_id,
                    'owner_id': owner_id,
                    'balance': balance,
                    'hold': hold,
                    'open': is_opened,
                }
            )

            account_obj = await cursor.fetchone()

            return AccountDTO(
                id=account_obj.id,
                owner=subscriber_dto,
                balance=account_obj.balance,
                hold=account_obj.hold,
                open=account_obj.open,
            )
