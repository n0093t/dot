from uuid import uuid4

from sqlalchemy import (
    Boolean,
    Column,
    ForeignKey,
    Index,
    MetaData,
    Numeric,
    String,
    Table,
)
from sqlalchemy.dialects.postgresql import UUID

metadata = MetaData()


subscriber = Table(
    'subscriber', metadata,
    Column('id', UUID(as_uuid=True), primary_key=True, default=uuid4),
    Column('full_name', String(length=255), nullable=False),
)

account = Table(
    'account', metadata,
    Column('id', UUID(as_uuid=True), primary_key=True, default=uuid4),
    Column(
        'owner_id',
        UUID(as_uuid=True),
        ForeignKey('subscriber.id'),
        nullable=False
    ),
    Column('balance', Numeric(), nullable=False),
    Column('hold', Numeric(), nullable=False),
    Column('open', Boolean, nullable=False),
    Index('ix_account_open_hold', 'open', 'hold'),
)
