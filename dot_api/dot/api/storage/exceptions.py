from typing import Optional

from dot.api.common.exceptions import BaseError


class AccountDoesNotExist(BaseError):
    __DEFAULT_MESSAGE = 'Account with id %s does not exist.'
    HTTP_STATUS = 404

    def __init__(self, account_id, message: Optional[str] = None):
        if message is None:
            message = self.__DEFAULT_MESSAGE % account_id

        self.account_id = account_id
        self.message = message

        super().__init__(message)


class SubscriberDoesNotExist(BaseError):
    __DEFAULT_MESSAGE = 'Subscriber with id %s does not exist.'
    HTTP_STATUS = 404

    def __init__(self, subscriber_id, message: Optional[str] = None):
        if message is None:
            message = self.__DEFAULT_MESSAGE % subscriber_id

        self.subscriber_id = subscriber_id
        self.message = message

        super().__init__(message)
