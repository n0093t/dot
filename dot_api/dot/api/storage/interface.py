from abc import ABC, abstractmethod
from contextlib import asynccontextmanager
from decimal import Decimal

from dot.api.common.dto import AccountDTO


class AbstractStorage(ABC):
    @abstractmethod
    async def get_account_by_id(self,
                                account_id,
                                for_update=False) -> AccountDTO:
        raise NotImplementedError

    @abstractmethod
    @asynccontextmanager
    async def atomic(self):
        raise NotImplementedError

    @abstractmethod
    async def add(self, account_id, amount: Decimal) -> AccountDTO:
        raise NotImplementedError

    @abstractmethod
    async def subtract(self, account_id, amount: Decimal):
        raise NotImplementedError

    @abstractmethod
    async def close(self):
        raise NotImplementedError
