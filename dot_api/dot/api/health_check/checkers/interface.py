from abc import ABC, abstractmethod


class IChecker(ABC):
    @property
    @abstractmethod
    def name(self):
        raise NotImplementedError

    @property
    @abstractmethod
    async def status(self):
        raise NotImplementedError
