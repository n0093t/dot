from contextlib import asynccontextmanager

import aiopg
from pydantic import PostgresDsn

from dot.api.health_check.checkers.interface import IChecker
from dot.api.health_check.status import ServiceStatus


class PostgresChecker(IChecker):
    def __init__(self, dsn: PostgresDsn):
        self.__dsn = dsn

    @asynccontextmanager
    async def __connection(self):
        async with aiopg.connect(self.__dsn, timeout=10) as conn:
            yield conn

    @property
    def name(self):
        return 'postgres@%s:%s%s' % (
            self.__dsn.host,
            self.__dsn.port or 5432,
            self.__dsn.path,
        )

    @property
    async def status(self) -> ServiceStatus:
        status = ServiceStatus.OK
        try:
            async with self.__connection() as conn, conn.cursor() as cur:
                await cur.execute('SELECT 1')
                await cur.fetchone()
        except Exception:   # pylint: disable=broad-except
            status = ServiceStatus.FAIL

        return status
