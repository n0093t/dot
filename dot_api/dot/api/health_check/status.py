from enum import Enum


class ServiceStatus(str, Enum):
    OK = 'OK'
    FAIL = 'FAIL'
