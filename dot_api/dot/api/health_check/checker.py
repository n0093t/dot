from typing import List

from dot.api.health_check.checkers.interface import IChecker
from dot.api.health_check.status import ServiceStatus


class ServiceAliveness:
    def __init__(self, name: str, alive: bool):
        self.name = name
        self.alive = alive


class AlivenessChecker:
    def __init__(self, checkers: List[IChecker]):
        self.__checkers = checkers

    async def services_statuses(self) -> List[ServiceAliveness]:
        result = []
        for checker in self.__checkers:
            status = await checker.status
            result.append(
                ServiceAliveness(
                    checker.name,
                    status == ServiceStatus.OK
                )
            )

        return result
