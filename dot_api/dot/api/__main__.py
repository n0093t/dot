from dot.api.runner import Runner
from dot.api.settings.api import APISettings
from dot.api.settings.db import PostgresSettings


if __name__ == '__main__':
    Runner(
        api_settings=APISettings(),
        storage_settings=PostgresSettings()
    ).run()
