from decimal import Decimal
from typing import List

from pydantic import BaseModel, Extra, UUID4


class BaseResponse(BaseModel):
    status: int
    result: bool

    class Config:
        extra = Extra.forbid


class AccountOwner(BaseModel):
    id: UUID4
    full_name: str


class AdditionalAccount(BaseModel):
    uuid: UUID4
    owner: AccountOwner
    balance: Decimal
    hold: Decimal
    open: bool


class Additional(BaseModel):
    account: AdditionalAccount


class AddBalanceResponse(BaseResponse):
    additional: Additional


class SubtractBalanceResponse(BaseResponse):
    additional: Additional


class AccountStatusResponse(BaseResponse):
    additional: Additional


class PingResponseServiceStatus(BaseModel):
    name: str
    alive: bool


class PingResponseDescription(BaseModel):
    services: List[PingResponseServiceStatus]


class PingResponse(BaseResponse):
    description: PingResponseDescription


class ErrorResponseDescription(BaseModel):
    errors: List[str]


class ErrorResponse(BaseResponse):
    description: ErrorResponseDescription
