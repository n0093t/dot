from pydantic import BaseModel, Extra, UUID4, condecimal


class BaseRequest(BaseModel):
    class Config:
        extra = Extra.forbid


class AddBalanceRequest(BaseRequest):
    class Params(BaseRequest):
        account_id: UUID4
        amount: condecimal(decimal_places=2, gt=0)

    params: Params


class SubtractBalanceRequest(BaseRequest):
    class Params(BaseRequest):
        account_id: UUID4
        amount: condecimal(decimal_places=2, gt=0)

    params: Params


class AccountStatusRequest(BaseRequest):
    class Params(BaseRequest):
        account_id: UUID4

    params: Params
