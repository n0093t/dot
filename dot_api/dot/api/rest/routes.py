from aiohttp import web

from dot.api.rest import handlers


ROUTES = [
    web.get('/api/ping', handlers.ping),
    web.post('/api/add', handlers.add_balance),
    web.post('/api/substract', handlers.subtract_balance),
    web.post('/api/status', handlers.account_status),
]
