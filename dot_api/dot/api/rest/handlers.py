from aiohttp import web

from dot.api.rest.encoder import custom_dumps
from dot.api.rest.schemas.request import (
    AccountStatusRequest,
    AddBalanceRequest,
    SubtractBalanceRequest,
)
from dot.api.rest.schemas.response import (
    AccountOwner,
    AccountStatusResponse,
    AddBalanceResponse,
    Additional,
    AdditionalAccount,
    PingResponse,
    PingResponseDescription,
    PingResponseServiceStatus,
    SubtractBalanceResponse,
)


async def ping(request):
    aliveness_checker = request.app['aliveness_checker']

    aliveness = True
    service_statuses = []
    for service_status in await aliveness_checker.services_statuses():
        if not service_status.alive:
            aliveness = False

        service_statuses.append(
            PingResponseServiceStatus(
                name=service_status.name,
                alive=service_status.alive,
            )
        )

    response_status = 200
    if not aliveness:
        response_status = 503

    response = PingResponse(
        status=response_status,
        result=response_status == 200,
        description=PingResponseDescription(services=service_statuses)
    )

    return web.json_response(
        response.dict(),
        status=response_status,
        dumps=custom_dumps
    )


async def add_balance(request):
    request_json = await request.json()
    request_params = AddBalanceRequest(**request_json).params

    account_service = request.app['account_service']

    account = await account_service.add_balance(
        account_id=request_params.account_id,
        amount=request_params.amount,
    )

    account_owner = AccountOwner(
        id=account.owner.id,
        full_name=account.owner.full_name
    )
    addition_account = AdditionalAccount(
        uuid=account.id,
        owner=account_owner,
        balance=account.balance,
        hold=account.hold,
        open=account.open,
    )
    additional = Additional(account=addition_account)

    response = AddBalanceResponse(
        status=200,
        result=True,
        additional=additional
    )

    return web.json_response(response.dict(), dumps=custom_dumps)


async def subtract_balance(request):
    request_json = await request.json()
    request_params = SubtractBalanceRequest(**request_json).params

    account_service = request.app['account_service']

    account = await account_service.subtract_balance(
        account_id=request_params.account_id,
        amount=request_params.amount,
    )

    account_owner = AccountOwner(
        id=account.owner.id,
        full_name=account.owner.full_name
    )

    addition_account = AdditionalAccount(
        uuid=account.id,
        owner=account_owner,
        balance=account.balance,
        hold=account.hold,
        open=account.open,
    )
    additional = Additional(account=addition_account)

    response = SubtractBalanceResponse(
        status=200,
        result=True,
        additional=additional
    )

    return web.json_response(response.dict(), dumps=custom_dumps)


async def account_status(request):
    request_json = await request.json()
    request_params = AccountStatusRequest(**request_json).params

    account_service = request.app['account_service']
    account = await account_service.account_status(request_params.account_id)

    account_owner = AccountOwner(
        id=account.owner.id,
        full_name=account.owner.full_name
    )

    addition_account = AdditionalAccount(
        uuid=account.id,
        owner=account_owner,
        balance=account.balance,
        hold=account.hold,
        open=account.open,
    )
    additional = Additional(account=addition_account)

    response = AccountStatusResponse(
        status=200,
        result=True,
        additional=additional
    )

    return web.json_response(response.dict(), dumps=custom_dumps)
