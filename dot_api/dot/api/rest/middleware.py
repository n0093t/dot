from json.decoder import JSONDecodeError

from aiohttp import web
from aiohttp.web_exceptions import HTTPException
from aiohttp.web_middlewares import middleware
from pydantic import ValidationError

from dot.api.common.exceptions import BaseError
from dot.api.common.utils import convert_pydantic_validation_error
from dot.api.rest.schemas.response import (
    ErrorResponse,
    ErrorResponseDescription
)


@middleware
async def error_middleware(request, handler):
    try:
        response = await handler(request)
    except BaseError as exc:
        errors = [exc.message]
        http_status = exc.HTTP_STATUS
    except ValidationError as exc:
        errors = convert_pydantic_validation_error(exc)
        http_status = 400
    except JSONDecodeError as exc:
        errors = [exc.msg]
        http_status = 400
    except HTTPException:
        raise
    except Exception:  # pylint: disable=broad-except
        errors = ['Unknown error']
        http_status = 500
    else:
        return response

    description = ErrorResponseDescription(errors=errors)
    data = ErrorResponse(
        status=http_status,
        result=False,
        description=description,
    )

    response = web.json_response(data.dict(), status=http_status)

    return response


MIDDLEWARES = [
    error_middleware,
]
