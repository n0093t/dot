from json import dumps, JSONEncoder
from typing import Any

from pydantic.json import pydantic_encoder


class CustomJSONEncoder(JSONEncoder):
    def default(self, o: Any) -> Any:  # pylint: disable=C0103
        return pydantic_encoder(o)


def custom_dumps(o: Any) -> Any:  # pylint: disable=C0103
    return dumps(o, cls=CustomJSONEncoder)
