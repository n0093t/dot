from decimal import Decimal
from typing import Optional
from uuid import uuid4

from dot.api.common.exceptions import BaseError


class OperationNotPermittedAccountClosed(BaseError):
    __DEFAULT_MESSAGE = 'Operation %s not permitted. Account %s closed.'
    HTTP_STATUS = 403

    def __init__(self, operation: str, account_id: uuid4,
                 message: Optional[str] = None):

        if message is None:
            message = self.__DEFAULT_MESSAGE % (operation, account_id)

        self.operation = operation
        self.account_id = account_id
        self.message = message

        super().__init__(message)


class InsufficientFunds(BaseError):
    __DEFAULT_MESSAGE = 'Account %s insufficient funds.'
    HTTP_STATUS = 400

    def __init__(self, account_id: uuid4, message: Optional[str] = None):
        if message is None:
            message = self.__DEFAULT_MESSAGE % account_id

        self.account_id = account_id
        self.message = message

        super().__init__(message)


class InvalidAmount(BaseError):
    __DEFAULT_MESSAGE = 'Invalid amount %s.'
    HTTP_STATUS = 400

    def __init__(self, amount: Decimal, message: Optional[str] = None):
        if message is None:
            message = self.__DEFAULT_MESSAGE % amount

        self.amount = amount
        self.message = message

        super().__init__(message)
