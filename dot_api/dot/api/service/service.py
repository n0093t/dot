from decimal import Decimal
from uuid import uuid4

from dot.api.common.dto import AccountDTO
from dot.api.service.exceptions import (
    InsufficientFunds,
    OperationNotPermittedAccountClosed,

)
from dot.api.service.exceptions import InvalidAmount
from dot.api.storage.interface import AbstractStorage


class AccountService:
    def __init__(self, storage: AbstractStorage):
        self.__storage = storage

    @staticmethod
    def __validate_amount(v: Decimal):
        _, _, exp = v.as_tuple()
        if v <= 0 or exp > 0 or exp < -2:
            raise InvalidAmount(v)

        return v

    async def add_balance(self, account_id: uuid4,
                          amount: Decimal) -> AccountDTO:

        self.__validate_amount(amount)

        async with self.__storage.atomic():
            account = await self.__storage.get_account_by_id(
                account_id,
                for_update=True
            )

            if not account.open:
                raise OperationNotPermittedAccountClosed(
                    '"Add balance"', account_id)

            return await self.__storage.add(account_id, amount)

    async def subtract_balance(self, account_id: uuid4,
                               amount: Decimal) -> AccountDTO:

        self.__validate_amount(amount)

        async with self.__storage.atomic():
            account = await self.__storage.get_account_by_id(
                account_id,
                for_update=True
            )

            if not account.open:
                raise OperationNotPermittedAccountClosed(
                    '"Subtract balance"', account_id)

            if account.hold + amount > account.balance:
                raise InsufficientFunds(account_id)

            return await self.__storage.subtract(account_id, amount)

    async def account_status(self, account_id: uuid4) -> AccountDTO:
        return await self.__storage.get_account_by_id(account_id)
