from pydantic import BaseSettings, PostgresDsn


class PostgresSettings(BaseSettings):
    dsn: PostgresDsn
    pool_min_size: int = 1
    pool_max_size: int = 20
    pool_recycle: int = 300
    timeout: int = 10

    class Config:
        env_prefix = 'dot_postgres_'
        env_file = '.env'
