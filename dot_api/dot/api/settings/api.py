from pydantic import BaseSettings


class APISettings(BaseSettings):
    host: str = '0.0.0.0'
    port: int = 8080

    class Config:
        env_prefix = 'dot_service_'
        env_file = '.env'
