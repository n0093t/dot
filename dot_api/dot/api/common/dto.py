from decimal import Decimal

from pydantic import BaseModel, UUID4


class SubscriberDTO(BaseModel):
    id: UUID4
    full_name: str


class AccountDTO(BaseModel):
    id: UUID4
    owner: SubscriberDTO
    balance: Decimal
    hold: Decimal
    open: bool
