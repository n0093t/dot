from typing import List

from pydantic import ValidationError


def convert_pydantic_validation_error(exc: ValidationError) -> List[str]:
    errors = []
    for error in exc.errors():
        location = ' > '
        location = location.join(error.get('loc', ''))
        msg = error['msg']
        if location:
            message = '%s: %s' % (location, msg)
        else:
            message = msg

        errors.append(message)

    return errors
