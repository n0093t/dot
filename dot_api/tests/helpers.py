from decimal import Decimal
from typing import Optional
from uuid import uuid4

from faker import Faker


fake = Faker()


async def create_fake_account(postgres_storage, *,
                              account_id: Optional[uuid4] = None,
                              owner_id: Optional[uuid4] = None,
                              balance: Optional[Decimal] = None,
                              hold: Optional[Decimal] = None,
                              is_opened: Optional[bool] = None):
    if owner_id is None:
        subscriber_obj = await postgres_storage.create_subscriber(
            subscriber_id=uuid4(),
            full_name=fake.name()
        )
        owner_id = subscriber_obj.id

    if is_opened is None:
        is_opened = fake.pybool()

    return await postgres_storage.create_account(
        account_id=account_id or uuid4(),
        owner_id=owner_id,
        balance=balance or fake.pydecimal(
            positive=True,
            min_value=1,
            right_digits=2,
        ),
        hold=hold or fake.pydecimal(
            positive=True,
            min_value=0,
            right_digits=2,
        ),
        is_opened=is_opened
    )
