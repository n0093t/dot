from uuid import uuid4

import pytest
from faker import Faker

from dot.api.storage.exceptions import AccountDoesNotExist
from tests.helpers import create_fake_account

fake = Faker()


@pytest.mark.asyncio
class TestPostgresStorage:
    async def test_get_account_by_id_ok(self, postgres_storage):
        expected_subscriber_id = uuid4()
        expected_subscriber_full_name = fake.name()

        expected_account_id = uuid4()
        expected_account_balance = fake.pydecimal(positive=True, min_value=1)
        expected_account_hold = fake.pydecimal(positive=True, min_value=1)
        expected_account_open = fake.pybool()

        async with postgres_storage.atomic() as (conn, trn):
            await postgres_storage.create_subscriber(
                subscriber_id=expected_subscriber_id,
                full_name=expected_subscriber_full_name,
            )

            await postgres_storage.create_account(
                account_id=expected_account_id,
                owner_id=expected_subscriber_id,
                balance=expected_account_balance,
                hold=expected_account_hold,
                is_opened=expected_account_open,
            )

            account_dto = await postgres_storage.get_account_by_id(
                expected_account_id
            )

            assert account_dto.id == expected_account_id
            assert account_dto.owner.id == expected_subscriber_id
            assert account_dto.owner.full_name == expected_subscriber_full_name
            assert account_dto.balance == expected_account_balance
            assert account_dto.hold == expected_account_hold
            assert account_dto.open == expected_account_open

            await trn.rollback()

    async def test_get_account_by_id_not_found(self, postgres_storage):
        account_id = uuid4()
        with pytest.raises(AccountDoesNotExist):
            await postgres_storage.get_account_by_id(account_id)

    async def test_add_account_balance(self, postgres_storage):
        async with postgres_storage.atomic() as (conn, trn):
            account_dto_1 = await create_fake_account(
                postgres_storage,
                is_opened=True
            )

            amount = fake.pydecimal(positive=True, min_value=0, right_digits=2)

            await postgres_storage.add(
                account_id=account_dto_1.id,
                amount=amount
            )

            account_dto_2 = await postgres_storage.get_account_by_id(
                account_dto_1.id
            )

            assert account_dto_2.balance == account_dto_1.balance + amount
            assert account_dto_2.hold == account_dto_1.hold

            await trn.rollback()

    async def test_subtract_account_balance(self, postgres_storage):
        async with postgres_storage.atomic() as (conn, trn):
            account_dto_1 = await create_fake_account(
                postgres_storage,
                is_opened=True
            )

            amount = fake.pydecimal(
                positive=True,
                min_value=0,
                max_value=int(account_dto_1.balance),
                right_digits=2
            )

            await postgres_storage.subtract(
                account_id=account_dto_1.id,
                amount=amount
            )

            account_dto_2 = await postgres_storage.get_account_by_id(
                account_dto_1.id
            )

            assert account_dto_2.balance == account_dto_1.balance
            assert account_dto_2.hold == account_dto_1.hold + amount

            await trn.rollback()
