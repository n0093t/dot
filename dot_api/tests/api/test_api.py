from uuid import uuid4

import pytest
from faker import Faker

from dot.api.common.dto import AccountDTO, SubscriberDTO
from dot.api.service.exceptions import InsufficientFunds
from dot.api.storage.exceptions import AccountDoesNotExist


@pytest.mark.asyncio
class TestAPI:
    @classmethod
    def setup_class(cls):
        cls.faker = Faker()

    async def test_add_balance(self, aiohttp_app, http_client):
        expected_account_id = uuid4()
        expected_balance = self.faker.pydecimal(
            positive=True,
            min_value=1,
            right_digits=2
        )
        expected_hold = self.faker.pydecimal(
            positive=True,
            min_value=1,
            right_digits=2
        )

        subscriber_dto = SubscriberDTO(id=uuid4(), full_name=self.faker.name())
        account_dto = AccountDTO(
            id=expected_account_id,
            owner=subscriber_dto,
            balance=expected_balance,
            hold=expected_hold,
            open=True
        )

        async def add_balance_mock(*args, **kwargs):
            return account_dto

        aiohttp_app['account_service'].add_balance = add_balance_mock

        response = await http_client.post(
            '/api/add',
            json={
                'params': {
                    'account_id': str(expected_account_id),
                    'amount': str(expected_balance),
                }
            },
        )
        response_json = await response.json()

        assert response.status == 200
        assert response_json['status'] == 200
        assert response_json['result'] is True

        account = response_json['additional']['account']
        assert account['uuid'] == str(account_dto.id)
        assert account['balance'] == float(account_dto.balance)
        assert account['hold'] == float(account_dto.hold)
        assert account['open'] == account_dto.open

        owner = account['owner']
        assert owner['id'] == str(account_dto.owner.id)
        assert owner['full_name'] == account_dto.owner.full_name

    async def test_add_balance_negative(self, http_client):
        expected_account_id = uuid4()
        expected_balance = self.faker.pydecimal(
            positive=True,
            min_value=1,
            right_digits=2
        )

        response = await http_client.post(
            '/api/add',
            json={
                'params': {
                    'account_id': str(expected_account_id),
                    'amount': str(-expected_balance),
                }
            },
        )

        response_json = await response.json()

        assert response.status == 400
        assert response_json['status'] == 400
        assert response_json['result'] is False

    async def test_add_balance_zero(self, http_client):
        expected_account_id = uuid4()

        response = await http_client.post(
            '/api/add',
            json={
                'params': {
                    'account_id': str(expected_account_id),
                    'amount': str(0),
                }
            },
        )

        response_json = await response.json()

        assert response.status == 400
        assert response_json['status'] == 400
        assert response_json['result'] is False

    async def test_add_balance_malformed_request(self, http_client):
        expected_account_id = uuid4()

        response = await http_client.post(
            '/api/add',
            json={
                'params': {
                    'account_id': str(expected_account_id),
                }
            },
        )

        response_json = await response.json()

        assert response.status == 400
        assert response_json['status'] == 400
        assert response_json['result'] is False

    async def test_subtract_balance(self, aiohttp_app, http_client):
        expected_account_id = uuid4()
        expected_balance = self.faker.pydecimal(
            positive=True,
            min_value=1,
            right_digits=2
        )
        expected_hold = self.faker.pydecimal(
            positive=True,
            min_value=1,
            right_digits=2
        )

        subscriber_dto = SubscriberDTO(id=uuid4(), full_name=self.faker.name())
        account_dto = AccountDTO(
            id=expected_account_id,
            owner=subscriber_dto,
            balance=expected_balance,
            hold=expected_hold,
            open=True
        )

        async def subtract_balance_mock(*args, **kwargs):
            return account_dto

        aiohttp_app['account_service'].subtract_balance = subtract_balance_mock

        response = await http_client.post(
            '/api/substract',
            json={
                'params': {
                    'account_id': str(expected_account_id),
                    'amount': str(expected_balance),
                }
            },
        )
        response_json = await response.json()

        assert response.status == 200
        assert response_json['status'] == 200
        assert response_json['result'] is True

        account = response_json['additional']['account']
        assert account['uuid'] == str(account_dto.id)
        assert account['balance'] == float(account_dto.balance)
        assert account['hold'] == float(account_dto.hold)
        assert account['open'] == account_dto.open

        owner = account['owner']
        assert owner['id'] == str(account_dto.owner.id)
        assert owner['full_name'] == account_dto.owner.full_name

    async def test_subtract_balance_negative(self, http_client):
        expected_account_id = uuid4()
        expected_balance = self.faker.pydecimal(
            positive=True,
            min_value=1,
            right_digits=2
        )

        response = await http_client.post(
            '/api/substract',
            json={
                'params': {
                    'account_id': str(expected_account_id),
                    'amount': str(-expected_balance),
                }
            },
        )

        response_json = await response.json()

        assert response.status == 400
        assert response_json['status'] == 400
        assert response_json['result'] is False

    async def test_subtract_balance_zero(self, http_client):
        expected_account_id = uuid4()

        response = await http_client.post(
            '/api/substract',
            json={
                'params': {
                    'account_id': str(expected_account_id),
                    'amount': str(0),
                }
            },
        )

        response_json = await response.json()

        assert response.status == 400
        assert response_json['status'] == 400
        assert response_json['result'] is False

    async def test_subtract_balance_malformed_request(self, http_client):
        expected_account_id = uuid4()

        response = await http_client.post(
            '/api/substract',
            json={
                'params': {
                    'account_id': str(expected_account_id),
                }
            },
        )

        response_json = await response.json()

        assert response.status == 400
        assert response_json['status'] == 400
        assert response_json['result'] is False

    async def test_subtract_balance_insufficient_funds(
        self,
        aiohttp_app,
        http_client
    ):
        expected_account_id = uuid4()
        subscriber_dto = SubscriberDTO(id=uuid4(), full_name=self.faker.name())
        account_dto = AccountDTO(
            id=uuid4(),
            owner=subscriber_dto,
            balance=100,
            hold=0,
            open=True
        )

        exc = InsufficientFunds(expected_account_id)
        aiohttp_app['account_service'].subtract_balance.side_effect = exc

        response = await http_client.post(
            '/api/substract',
            json={
                'params': {
                    'account_id': str(expected_account_id),
                    'amount': str(100),
                }
            },
        )
        response_json = await response.json()

        assert response.status == 400
        assert response_json['status'] == 400
        assert response_json['result'] is False

    async def test_account_status(self, aiohttp_app, http_client):
        expected_account_id = uuid4()
        expected_balance = self.faker.pydecimal(
            positive=True,
            min_value=1,
            right_digits=2
        )
        expected_hold = self.faker.pydecimal(
            positive=True,
            min_value=1,
            right_digits=2
        )

        subscriber_dto = SubscriberDTO(id=uuid4(), full_name=self.faker.name())
        account_dto = AccountDTO(
            id=expected_account_id,
            owner=subscriber_dto,
            balance=expected_balance,
            hold=expected_hold,
            open=True
        )

        async def account_status_mock(*args, **kwargs):
            return account_dto

        aiohttp_app['account_service'].account_status = account_status_mock

        response = await http_client.post(
            '/api/status',
            json={
                'params': {
                    'account_id': str(expected_account_id),
                }
            },
        )
        response_json = await response.json()

        assert response.status == 200
        assert response_json['status'] == 200
        assert response_json['result'] is True

        account = response_json['additional']['account']
        assert account['uuid'] == str(account_dto.id)
        assert account['balance'] == float(account_dto.balance)
        assert account['hold'] == float(account_dto.hold)
        assert account['open'] == account_dto.open

        owner = account['owner']
        assert owner['id'] == str(account_dto.owner.id)
        assert owner['full_name'] == account_dto.owner.full_name

    async def test_account_status_not_found(self, aiohttp_app, http_client):
        expected_account_id = uuid4()
        subscriber_dto = SubscriberDTO(id=uuid4(), full_name=self.faker.name())
        account_dto = AccountDTO(
            id=expected_account_id,
            owner=subscriber_dto,
            balance=0,
            hold=0,
            open=True
        )

        exc = AccountDoesNotExist(expected_account_id)
        aiohttp_app['account_service'].account_status.side_effect = exc

        response = await http_client.post(
            '/api/status',
            json={
                'params': {
                    'account_id': str(expected_account_id),
                }
            },
        )
        response_json = await response.json()

        assert response.status == 404
        assert response_json['status'] == 404
        assert response_json['result'] is False
