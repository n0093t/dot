from unittest.mock import MagicMock

import pytest
from aiohttp import web

from dot.api.rest.middleware import MIDDLEWARES
from dot.api.rest.routes import ROUTES


@pytest.fixture
def aiohttp_app():
    app = web.Application(middlewares=MIDDLEWARES)
    app.router.add_routes(ROUTES)

    app['account_service'] = MagicMock()

    return app


@pytest.fixture
def http_client(loop, aiohttp_app, aiohttp_client):
    return loop.run_until_complete(aiohttp_client(aiohttp_app))
