import pytest

from dot.api.settings.db import PostgresSettings
from dot.api.storage.postgres.storage import PostgresStorage


@pytest.fixture
async def postgres_storage():
    postgres_settings = PostgresSettings()
    storage = PostgresStorage(postgres_settings.dsn)
    try:
        yield storage
    finally:
        await storage.close()
