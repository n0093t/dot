from decimal import Decimal

import pytest
from faker import Faker

from dot.api.service import AccountService
from dot.api.service.exceptions import (
    InsufficientFunds, InvalidAmount,
    OperationNotPermittedAccountClosed,
)
from tests.helpers import create_fake_account

fake = Faker()


@pytest.mark.asyncio
class TestAccountService:
    async def test_add_balance(self, postgres_storage):
        async with postgres_storage.atomic() as (conn, trn):
            account_dto_1 = await create_fake_account(
                postgres_storage,
                is_opened=True
            )
            account_service = AccountService(postgres_storage)

            amount = fake.pydecimal(positive=True, min_value=1, right_digits=2)
            account_dto_2 = await account_service.add_balance(
                account_id=account_dto_1.id,
                amount=amount
            )

            assert account_dto_2.balance == account_dto_1.balance + amount
            assert account_dto_2.hold == account_dto_1.hold

            await trn.rollback()

    async def test_add_balance_negative(self, postgres_storage):
        async with postgres_storage.atomic() as (conn, trn):
            account_service = AccountService(postgres_storage)
            account_dto = await create_fake_account(
                postgres_storage,
                is_opened=True
            )

            with pytest.raises(InvalidAmount):
                await account_service.add_balance(
                    account_id=account_dto.id,
                    amount=Decimal('-1.23')
                )

            await trn.rollback()

    async def test_add_balance_invalid_precision(self, postgres_storage):
        async with postgres_storage.atomic() as (conn, trn):
            account_service = AccountService(postgres_storage)
            account_dto = await create_fake_account(
                postgres_storage,
                is_opened=True
            )

            with pytest.raises(InvalidAmount):
                await account_service.add_balance(
                    account_id=account_dto.id,
                    amount=Decimal('1.235')
                )

            await trn.rollback()

    async def test_add_balance_zero(self, postgres_storage):
        async with postgres_storage.atomic() as (conn, trn):
            account_service = AccountService(postgres_storage)
            account_dto = await create_fake_account(
                postgres_storage,
                is_opened=True
            )

            with pytest.raises(InvalidAmount):
                await account_service.add_balance(
                    account_id=account_dto.id,
                    amount=Decimal('0')
                )

            await trn.rollback()

    async def test_add_balance_closed_account(self, postgres_storage):
        async with postgres_storage.atomic() as (conn, trn):
            account_service = AccountService(postgres_storage)
            account_dto_1 = await create_fake_account(
                postgres_storage,
                is_opened=False
            )

            with pytest.raises(OperationNotPermittedAccountClosed):
                await account_service.add_balance(
                    account_id=account_dto_1.id,
                    amount=Decimal('1')
                )

                account_dto_2 = await postgres_storage.get_account_by_id(
                    account_dto_1.id
                )

                assert account_dto_2.balance == account_dto_1.balance
                assert account_dto_2.hold == account_dto_2.hold

            await trn.rollback()

    async def test_subtract_balance(self, postgres_storage):
        async with postgres_storage.atomic() as (conn, trn):
            account_dto_1 = await create_fake_account(
                postgres_storage,
                balance=Decimal('100'),
                hold=Decimal('50'),
                is_opened=True
            )
            account_service = AccountService(postgres_storage)

            amount = fake.pydecimal(
                positive=True,
                min_value=1,
                max_value=Decimal('50'),
                right_digits=2
            )

            account_dto_2 = await account_service.subtract_balance(
                account_id=account_dto_1.id,
                amount=amount
            )

            assert account_dto_2.hold == account_dto_1.hold + amount
            assert account_dto_2.balance == account_dto_1.balance

            await trn.rollback()

    async def test_subtract_balance_negative(self, postgres_storage):
        async with postgres_storage.atomic() as (conn, trn):
            account_service = AccountService(postgres_storage)
            account_dto = await create_fake_account(
                postgres_storage,
                is_opened=True
            )

            with pytest.raises(InvalidAmount):
                await account_service.subtract_balance(
                    account_id=account_dto.id,
                    amount=Decimal('-1.23')
                )

            await trn.rollback()

    async def test_subtract_balance_zero(self, postgres_storage):
        async with postgres_storage.atomic() as (conn, trn):
            account_service = AccountService(postgres_storage)
            account_dto = await create_fake_account(
                postgres_storage,
                is_opened=True
            )

            with pytest.raises(InvalidAmount):
                await account_service.subtract_balance(
                    account_id=account_dto.id,
                    amount=Decimal('0')
                )

            await trn.rollback()

    async def test_subtract_balance_invalid_precision(self, postgres_storage):
        async with postgres_storage.atomic() as (conn, trn):
            account_service = AccountService(postgres_storage)
            account_dto = await create_fake_account(
                postgres_storage,
                is_opened=True
            )

            with pytest.raises(InvalidAmount):
                await account_service.subtract_balance(
                    account_id=account_dto.id,
                    amount=Decimal('1.235')
                )

            await trn.rollback()

    async def test_subtract_balance_closed_account(self, postgres_storage):
        async with postgres_storage.atomic() as (conn, trn):
            account_service = AccountService(postgres_storage)
            account_dto_1 = await create_fake_account(
                postgres_storage,
                is_opened=False
            )

            with pytest.raises(OperationNotPermittedAccountClosed):
                await account_service.subtract_balance(
                    account_id=account_dto_1.id,
                    amount=Decimal('1')
                )

                account_dto_2 = await postgres_storage.get_account_by_id(
                    account_dto_1.id
                )

                assert account_dto_2.balance == account_dto_1.balance
                assert account_dto_2.hold == account_dto_2.hold

            await trn.rollback()

    async def test_subtract_balance_insufficient_funds(self, postgres_storage):
        async with postgres_storage.atomic() as (conn, trn):
            account_service = AccountService(postgres_storage)
            account_dto_1 = await create_fake_account(
                postgres_storage,
                balance=Decimal('100'),
                hold=Decimal('50'),
                is_opened=True
            )

            with pytest.raises(InsufficientFunds):
                await account_service.subtract_balance(
                    account_id=account_dto_1.id,
                    amount=Decimal('50.23')
                )

                account_dto_2 = await postgres_storage.get_account_by_id(
                    account_dto_1.id
                )

                assert account_dto_2.balance == account_dto_1.balance
                assert account_dto_2.hold == account_dto_2.hold

            await trn.rollback()

    async def test_account_status(self, postgres_storage):
        async with postgres_storage.atomic() as (conn, trn):
            account_service = AccountService(postgres_storage)
            account_dto_1 = await create_fake_account(postgres_storage)

            account_dto_2 = await account_service.account_status(
                account_dto_1.id
            )

            assert account_dto_2.id == account_dto_1.id
            assert account_dto_2.owner.id == account_dto_1.owner.id
            assert (account_dto_2.owner.full_name ==
                    account_dto_1.owner.full_name)
            assert account_dto_2.balance == account_dto_1.balance
            assert account_dto_2.hold == account_dto_1.hold
            assert account_dto_2.open == account_dto_1.open

            await trn.rollback()
