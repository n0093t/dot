#!/usr/bin/env bash

function build {
  if [ ${#@} -eq 0 ];
  then
    services=('api' 'consumer' 'scheduler')
  else
    services=( "$@" )
  fi

  docker-compose build "${services[@]}"
}

function test {
  if [ ${#@} -eq 0 ];
  then
    echo 'Specify service name'
    exit
  fi

  docker-compose run --rm "$1" pytest
}

function logs {
  if [ ${#@} -eq 0 ];
  then
    echo 'Specify service name'
    exit
  fi

  docker-compose logs "$1"
}

function down {
  docker-compose down
}

function stop {
  if [ ${#@} -eq 0 ];
  then
    services=('nginx' 'api' 'consumer' 'scheduler' 'postgres' 'rabbitmq')
  else
    services=( "$@" )
  fi

  docker-compose stop "$services"
}

function up {
  docker-compose up -d
}

function main {
  if [ $# -eq 0 ];
  then
    echo "$usage"
    exit
  fi

  case "$1" in
    build) build "${@:2}";;
    up) up;;
    down) down;;
    stop) stop "${@:2}";;
    test) test "${@:2}";;
    logs) logs "${@:2}";;
    *) echo "Unknown command: $1"
  esac
}


script=$(basename "$0")
usage="Usage:
  $script build [service_name service_name ...]
  $script up
  $script down
  $script stop [service_name service_name ...]
  $script test [service_name]
  $script logs service_name"

main "$@"
