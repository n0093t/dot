#!/usr/bin/env bash

function wait_for_postgres {
  while true;
  do
    exec 3>&2 2>/dev/null
    echo -n "Check connection postgres@$DOT_POSTGRES_HOST..........................."
    exec 4<>/dev/tcp/"$DOT_POSTGRES_HOST"/5432
    connected=$?
    exec 2>&3 3>&-

    if [ $connected -eq 0 ];
    then
      echo '[OK]'
      exec 4>&-
      exec 4<&-
      break
    else
      echo '[ERROR]'
      sleep 1s
    fi
  done
}

wait_for_postgres

cd /tmp/dot_api/dot/api/ || exit
alembic upgrade head
cd /tmp/dot_api/ || exit

exec "$@"
