import signal

from apscheduler.schedulers.blocking import BlockingScheduler

from dot.scheduler.amqp.publisher import JobPublisher
from dot.scheduler.common.exceptions import (
    GracefulShutdown,
    raise_graceful_shutdown
)
from dot.scheduler.jobs import SubtractBalanceMessage, job_wrapper
from dot.scheduler.settings.jobs import SubtractBalanceJobSettings
from dot.scheduler.settings.rabbitmq import RabbitMQSettings
from dot.scheduler.settings.scheduler import SchedulerSettings


class Runner:
    def __init__(self, scheduler_settings: SchedulerSettings,
                 rabbitmq_settings: RabbitMQSettings,
                 subtract_balance_job_settings: SubtractBalanceJobSettings):
        self.__scheduler_settings = scheduler_settings
        self.__rabbitmq_settings = rabbitmq_settings
        self.__subtract_balance_job_settings = subtract_balance_job_settings
        self.__job_publisher = None
        self.__scheduler = None

    @staticmethod
    def __setup_signals():
        signal.signal(signal.SIGINT, raise_graceful_shutdown)
        signal.signal(signal.SIGTERM, raise_graceful_shutdown)

    def __setup_job_publisher(self):
        self.__job_publisher = JobPublisher(**self.__rabbitmq_settings.dict())

    def __setup_scheduler(self):
        self.__scheduler = BlockingScheduler(
            **self.__scheduler_settings.dict(by_alias=True)
        )

    def __setup_jobs(self):
        self.__scheduler.add_job(
            job_wrapper,
            args=(SubtractBalanceMessage(), self.__job_publisher),
            coalesce=True,
            **self.__subtract_balance_job_settings.dict(),
        )

    def run(self):
        self.__setup_job_publisher()
        self.__setup_scheduler()
        self.__setup_jobs()
        self.__setup_signals()

        try:
            self.__scheduler.start()
        except (GracefulShutdown, KeyboardInterrupt):
            pass
        finally:
            if self.__scheduler.running:
                self.__scheduler.shutdown()
