from pydantic import BaseModel


class SubtractBalanceMessage(BaseModel):
    operation: str = 'SubtractBalance'
