from pydantic import BaseModel

from dot.scheduler.amqp.publisher import JobPublisher


def job_wrapper(message: BaseModel, job_publisher: JobPublisher):
    job_publisher.publish(message.json())
