from pydantic import BaseSettings


class SubtractBalanceJobSettings(BaseSettings):
    id: str = 'SubtractBalance'
    replace_existing: bool = True
    trigger: str = 'interval'
    weeks: int = 0
    days: int = 0
    hours: int = 0
    minutes: int = 0
    seconds: int = 0

    class Config:
        env_prefix = 'dot_scheduler_subtract_balance_job_settings_'
        env_file = '.env'
