from pydantic import BaseSettings, PostgresDsn, Field


class JobStore(BaseSettings):
    type: str
    url: PostgresDsn

    class Config:
        env_prefix = 'dot_scheduler_job_store_'
        env_file = '.env'


class SchedulerSettings(BaseSettings):
    job_store: JobStore = Field(
        JobStore(),
        alias='apscheduler.jobstores.default'
    )
    timezone: str = Field('UTC', alias='apscheduler.timezone')

    class Config:
        env_prefix = 'dot_scheduler_'
        env_file = '.env'
