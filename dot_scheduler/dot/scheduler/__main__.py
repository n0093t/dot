from dot.scheduler.runner import Runner
from dot.scheduler.settings.jobs import SubtractBalanceJobSettings
from dot.scheduler.settings.rabbitmq import RabbitMQSettings
from dot.scheduler.settings.scheduler import SchedulerSettings


if __name__ == '__main__':
    runner = Runner(
        scheduler_settings=SchedulerSettings(),
        rabbitmq_settings=RabbitMQSettings(),
        subtract_balance_job_settings=SubtractBalanceJobSettings(),
    )
    runner.run()
