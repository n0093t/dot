from pika import BasicProperties, BlockingConnection, ConnectionParameters
from pika.exceptions import AMQPConnectionError, ConnectionClosedByBroker


class JobPublisher:
    def __init__(self, host: str, port: int, exchange: str, queue: str,
                 routing_key: str, connect_timeout=10):

        self.__connection_parameters = ConnectionParameters(
            host=host,
            port=port,
            socket_timeout=connect_timeout,
        )

        self.__connection = None
        self.__channel = None
        self.__exchange = exchange
        self.__queue = queue
        self.__exchange_dl = '%s-dlx' % self.__exchange
        self.__queue_dl = '%s-dl' % self.__queue
        self.__routing_key = routing_key
        self.__connection_initialized = False

    def __connect(self):
        self.__connection = BlockingConnection(self.__connection_parameters)

    def __setup_channel(self):
        self.__channel = self.__connection.channel()

    def __setup_queue_dl(self):
        self.__channel.exchange_declare(self.__exchange_dl)
        self.__channel.queue_declare(self.__queue_dl, durable=True)
        self.__channel.queue_bind(
            self.__queue_dl,
            self.__exchange_dl,
            self.__routing_key,
        )

    def __setup_queue(self):
        self.__channel.exchange_declare(self.__exchange)
        self.__channel.queue_declare(
            self.__queue,
            durable=True,
            arguments={
                'x-dead-letter-exchange': self.__exchange_dl,
                'x-dead-letter-routing-key': self.__routing_key,
            }
        )

        self.__channel.queue_bind(
            self.__queue,
            self.__exchange,
            self.__routing_key
        )

    def connect(self):
        if self.__connection_initialized:
            return

        self.__connect()
        self.__setup_channel()
        self.__setup_queue_dl()
        self.__setup_queue()
        self.__connection_initialized = True

    def publish(self, message: str):
        properties = BasicProperties(content_type='application/json')

        while True:
            try:
                self.connect()
                self.__channel.basic_publish(
                    self.__exchange,
                    self.__routing_key,
                    message,
                    properties=properties,
                )
            except (AMQPConnectionError, ConnectionClosedByBroker):
                self.__connection_initialized = False
                continue
            else:
                break

    def close(self):
        if self.__connection is None:
            return

        self.__connection.close()
