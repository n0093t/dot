class GracefulShutdown(SystemExit):
    pass


def raise_graceful_shutdown(sig_num, frame):
    raise GracefulShutdown
