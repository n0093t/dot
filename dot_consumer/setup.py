import setuptools


def __get_requirements():
    requirements = []
    with open('requirements.txt') as f:
        requirements.extend(f.readlines())

    with open('requirements-dev.txt') as f:
        requirements.extend(f.readlines())

    return requirements


setuptools.setup(
    name='dot-consumer',
    version='0.0.1',
    author='Gasnikov Dmitriy',
    install_requires=__get_requirements(),
    packages=setuptools.find_namespace_packages(include='dot.*')
)
