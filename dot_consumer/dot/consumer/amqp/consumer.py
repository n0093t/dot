import json

from pika import BlockingConnection, ConnectionParameters
from pika.exceptions import AMQPConnectionError, ConnectionClosedByBroker

from dot.consumer.storage.interface import AbstractStorage


class Consumer:
    def __init__(self, host: str, port: int, exchange: str, queue: str,
                 routing_key: str, storage: AbstractStorage):

        self.__connection_parameters = ConnectionParameters(
            host=host,
            port=port
        )
        self.__connection = None
        self.__channel = None
        self.__exchange = exchange
        self.__queue = queue
        self.__exchange_dl = '%s-dlx' % self.__exchange
        self.__queue_dl = '%s-dl' % self.__queue
        self.__routing_key = routing_key
        self.__storage = storage

    def __on_message(self, channel, method, properties, body):  # pylint: disable=W0613
        try:
            body_json = json.loads(body)
            operation = body_json['operation']
            if operation == 'SubtractBalance':
                self.__storage.process_hold()

            channel.basic_ack(delivery_tag=method.delivery_tag)
        except Exception:  # pylint: disable=W0703
            channel.basic_reject(
                delivery_tag=method.delivery_tag,
                requeue=False
            )

    def __setup_connection(self):
        self.__connection = BlockingConnection(self.__connection_parameters)

    def __setup_channel(self):
        self.__channel = self.__connection.channel()
        self.__channel.basic_qos(prefetch_count=1)

    def __setup_queue_dl(self):
        self.__channel.exchange_declare(self.__exchange_dl)
        self.__channel.queue_declare(self.__queue_dl, durable=True)
        self.__channel.queue_bind(
            self.__queue_dl,
            self.__exchange_dl,
            self.__routing_key,
        )

    def __setup_queue(self):
        self.__channel.exchange_declare(self.__exchange)
        self.__channel.queue_declare(
            self.__queue,
            durable=True,
            arguments={
                'x-dead-letter-exchange': self.__exchange_dl,
                'x-dead-letter-routing-key': self.__routing_key,
            }
        )
        self.__channel.queue_bind(
            self.__queue,
            self.__exchange,
            self.__routing_key
        )

    def start_consuming(self):
        while True:
            try:
                self.__setup_connection()
                self.__setup_channel()
                self.__setup_queue_dl()
                self.__setup_queue()

                self.__channel.basic_consume(self.__queue, self.__on_message)
                self.__channel.start_consuming()
            except (AMQPConnectionError, ConnectionClosedByBroker):
                continue

    def stop_consuming(self):
        self.__channel.stop_consuming()
        self.__connection.close()
