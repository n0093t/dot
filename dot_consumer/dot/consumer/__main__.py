from dot.consumer.runner import Runner
from dot.consumer.settings.rabbitmq import RabbitMQSettings
from dot.consumer.settings.db import PostgresSettings


if __name__ == '__main__':
    consumer_settings = RabbitMQSettings()
    postgres_settings = PostgresSettings()
    runner = Runner(
        consumer_settings=consumer_settings,
        storage_settings=postgres_settings,
    )
    runner.run()
