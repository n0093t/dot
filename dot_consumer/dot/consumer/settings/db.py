from pydantic import BaseSettings


class PostgresSettings(BaseSettings):
    host: str
    port: int
    database: str
    user: str
    password: str

    class Config:
        env_prefix = 'dot_consumer_postgres_'
        env_file = '.env'
