from pydantic import BaseSettings


class RabbitMQSettings(BaseSettings):
    host: str = '0.0.0.0'
    port: int = 5672
    exchange: str
    routing_key: str
    queue: str

    class Config:
        env_prefix = 'dot_consumer_rabbitmq_'
        env_file = '.env'
