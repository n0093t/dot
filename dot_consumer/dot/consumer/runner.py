import signal

from dot.consumer.amqp.consumer import Consumer
from dot.consumer.common.exceptions import (
    GracefulShutdown,
    raise_graceful_shutdown
)
from dot.consumer.settings.db import PostgresSettings
from dot.consumer.settings.rabbitmq import RabbitMQSettings
from dot.consumer.storage.postgres.storage import PostgresStorage


class Runner:
    def __init__(self, consumer_settings: RabbitMQSettings,
                 storage_settings: PostgresSettings):

        self.__consumer_settings = consumer_settings
        self.__storage_settings = storage_settings
        self.__consumer = None
        self.__storage = None

    @staticmethod
    def __setup_signals():
        signal.signal(signal.SIGINT, raise_graceful_shutdown)
        signal.signal(signal.SIGTERM, raise_graceful_shutdown)

    def __setup_storage(self):
        self.__storage = PostgresStorage(
            host=self.__storage_settings.host,
            port=self.__storage_settings.port,
            database=self.__storage_settings.database,
            user=self.__storage_settings.user,
            password=self.__storage_settings.password
        )

    def __setup_consumer(self):
        self.__consumer = Consumer(
            host=self.__consumer_settings.host,
            port=self.__consumer_settings.port,
            exchange=self.__consumer_settings.exchange,
            queue=self.__consumer_settings.queue,
            routing_key=self.__consumer_settings.routing_key,
            storage=self.__storage,
        )

    def run(self):
        self.__setup_signals()
        self.__setup_storage()
        self.__setup_consumer()

        try:
            self.__consumer.start_consuming()
        except (GracefulShutdown, KeyboardInterrupt):
            pass
        finally:
            self.__consumer.stop_consuming()
