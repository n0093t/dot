import psycopg2

from dot.consumer.storage.interface import AbstractStorage


class PostgresStorage(AbstractStorage):
    def __init__(self, host: str, port: int, database: str, user: str,
                 password: str):
        self.__host = host
        self.__port = port
        self.__database = database
        self.__user = user
        self.__password = password

    def process_hold(self):
        query = '''
        UPDATE account
            SET balance = balance - "hold",
                "hold" = 0
            WHERE "open" IS TRUE AND "hold" > 0
        '''

        connection = psycopg2.connect(
            host=self.__host,
            port=self.__port,
            database=self.__database,
            user=self.__user,
            password=self.__password
        )

        cursor = connection.cursor()
        cursor.execute(query)
        connection.commit()
        cursor.close()
        connection.close()
