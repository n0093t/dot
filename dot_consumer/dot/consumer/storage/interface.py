from abc import ABC


class AbstractStorage(ABC):
    def process_hold(self):
        raise NotImplementedError
