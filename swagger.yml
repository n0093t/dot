openapi: 3.0.0
info:
  title: API
  description: Описание взаимодействия со счетами пользователей через API
  version: 0.0.1
servers:
  - url: http://localhost/
    description: localhost
paths:
  /api/ping:
    get:
      tags:
        - api
      summary: Возвращает информацию о состоянии сервиса
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                type: object
                properties:
                  status:
                    type: integer
                    description: HTTP статус
                    example: 200
                  result:
                    type: boolean
                    description: Результат выполнения операции
                  description:
                    type: object
                    properties:
                      services:
                        type: array
                        items:
                          type: object
                          properties:
                            name:
                              type: string
                              description: Сервис
                              example: "postgres@postgres:5432/dot"
                            alive:
                              type: boolean
                              description: Статус сервиса
        '503':
          description: FAIL
          content:
            application/json:
              schema:
                type: object
                properties:
                  status:
                    type: integer
                    description: HTTP статус
                    example: 503
                  result:
                    type: boolean
                    description: Результат выполнения операции
                    example: false
                  description:
                    type: object
                    properties:
                      services:
                        type: array
                        items:
                          type: object
                          properties:
                            name:
                              type: string
                              description: Сервис
                              example: "postgres@postgres:5432/dot"
                            alive:
                              type: boolean
                              description: Статус сервиса
                              example: false
  /api/status:
    post:
      summary: Получение информации о счете
      tags:
        - api
      requestBody:
        required: true
        content:
          application/json:
            schema:
              type: object
              properties:
                params:
                  type: object
                  properties:
                    account_id:
                      type: string
                      description: Идентификатор счета
                      example: "867f0924-a917-4711-939b-90b179a96392"
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                type: object
                properties:
                  status:
                    type: integer
                    description: HTTP статус
                    example: 200
                  result:
                    type: boolean
                    description: Результат выполнения операции
                    example: true
                  additional:
                    $ref: '#/components/schemas/account'
        '404':
          description: OK
          content:
            application/json:
              schema:
                type: object
                properties:
                  status:
                    type: integer
                    description: HTTP статус
                    example: 404
                  result:
                    type: boolean
                    description: Результат выполнения операции
                    example: false
                  description:
                    type: object
                    properties:
                      errors:
                        type: array
                        items:
                          $ref: '#/components/schemas/account_not_found'

  /api/add:
    post:
      summary: Пополнение баланса
      tags:
        - api
      requestBody:
        required: true
        content:
          application/json:
            schema:
              type: object
              properties:
                params:
                  type: object
                  properties:
                    account_id:
                      type: string
                      description: Идентификатор счета
                      example: "867f0924-a917-4711-939b-90b179a96392"
                    amount:
                      type: number
                      description: Сумма
                      example: 100.45
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                type: object
                properties:
                  status:
                    type: integer
                    description: HTTP статус
                    example: 200
                  result:
                    type: boolean
                    description: Результат выполнения операции
                    example: true
                  additional:
                    $ref: '#/components/schemas/account'
        '403':
          description: Операция запрещена
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/account_closed'
        '404':
          description: Аккаунт не найден
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/account_not_found'

  /api/substract:
    post:
      summary: Снятие д/с
      tags:
        - api
      requestBody:
        required: true
        content:
          application/json:
            schema:
              type: object
              properties:
                params:
                  type: object
                  properties:
                    account_id:
                      type: string
                      description: Идентификатор счета
                      example: "867f0924-a917-4711-939b-90b179a96392"
                    amount:
                      type: number
                      description: Сумма
                      example: 100.45
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                type: object
                properties:
                  status:
                    type: integer
                    description: HTTP статус
                    example: 200
                  result:
                    type: boolean
                    description: Результат выполнения операции
                    example: true
                  additional:
                    $ref: '#/components/schemas/account'
        '400':
          description: Недостаточно д/с
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/insufficient_funds'
        '403':
          description: Операция запрещена
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/account_closed'
        '404':
          description: Аккаунт не найден
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/account_not_found'


components:
  schemas:
    insufficient_funds:
      type: object
      properties:
        status:
          type: integer
          description: HTTP статус
          example: 404
        result:
          type: boolean
          description: Результат выполнения операции
          example: false
        description:
          type: object
          properties:
            errors:
              type: array
              items:
                type: string
                description: Описание ошибки
                example: "Account 26c940a1-7228-4ea2-a3bc-e6460b172040 insufficient funds."
    account_closed:
      type: object
      properties:
        status:
          type: integer
          description: HTTP статус
          example: 404
        result:
          type: boolean
          description: Результат выполнения операции
          example: false
        description:
          type: object
          properties:
            errors:
              type: array
              items:
                type: string
                description: Описание ошибки
                example: "Operation \"Add balance\" not permitted. Account 867f0924-a917-4711-939b-90b179a96392 closed."
    account_not_found:
      type: object
      properties:
        status:
          type: integer
          description: HTTP статус
          example: 404
        result:
          type: boolean
          description: Результат выполнения операции
          example: false
        description:
          type: object
          properties:
            errors:
              type: array
              items:
                type: string
                description: Описание ошибки
                example: "Account with id 267f0924-a917-4711-939b-90b179a96392 does not exist."
    account_owner:
      type: object
      properties:
        id:
          type: string
          description: Идентификатор владельца аккаунта
          example: "74b0bb75-45d3-458c-a315-863009693a65"
        full_name:
          type: string
          description: ФИО
          example: Петечкин Петр Измаилович
    account:
      type: object
      properties:
        uuid:
          type: string
          description: Идентификтаор аккаунта
          example: "867f0924-a917-4711-939b-90b179a96392"
        owner:
          $ref: '#/components/schemas/account_owner'
        balance:
          type: number
          description: Баланс
          example: 123.45
        hold:
          type: number
          description: Холд
          example: 100.12
        open:
          type: boolean
          description: Статус счета. Открыт/Закрыт.
          example: true
